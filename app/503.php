<?php
/**
 * The MIT License (MIT)
 *
  * Copyright (c) 2016 
 * Andreas Hafner <a.hafner@teufels.com>, 
 * Dominik Hilser <d.hilser@teufels.com>, 
 * Georg Kathan <g.kathan@teufels.com>, 
 * Hendrik Krüger <h.krueger@teufels.com>,
 * Perrin Ennen <p.ennen@teufels.com>, 
 * Timo Bittner <t.bittner@teufels.com>,
 * teufels GmbH <digital@teufels.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

$GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_force'] = 1;
if ($GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_force'] == 1) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '217.92.80.226,37.247.80.136,37.200.100.39,134.119.41.40';
}

// Fallback in EXT:teufels_thm_error
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_handling_redirectFallback'] = '/typo3conf/ext/teufels_thm_error/Resources/Public/503/';

// USER_FUNCTION
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_handling'] = 'USER_FUNCTION:typo3conf/ext/teufels_thm_error/Classes/Utility/PageUnavailableHandling.php:TEUFELS\TeufelsThmError\Utility\PageUnavailableHandling->pageUnavailable';

