php web/typo3/cli_dispatch.phpsh  extbase extension:install flux
php web/typo3/cli_dispatch.phpsh  extbase extension:install fluidcontent
php web/typo3/cli_dispatch.phpsh  extbase extension:install vhs

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_page
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_typoscript
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_user
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_brand

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_btn
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_carousel
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_collapsible
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_coltwo
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_falimg
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_tab
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_falimg

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_dynamiccontent
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_anchor
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_breadcrumb
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_mega
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_mobile
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_simple
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_simple_dd
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_special
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_sub

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_fluidstyledcontent
php web/typo3/cli_dispatch.phpsh  extbase extension:install metaseo
php web/typo3/cli_dispatch.phpsh  extbase extension:install cooluri
php web/typo3/cli_dispatch.phpsh  extbase extension:install powermail
php web/typo3/cli_dispatch.phpsh  extbase extension:install news
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_metaseo
php web/typo3/cli_dispatch.phpsh  extbase extension:install sourceopt
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_sourceopt

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_less
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_backendlayout
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_blazy
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_bs
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_bs_toolkit
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_custom
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_ie_dinosaurs
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_anima
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_echo
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_focuspoint
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_hammer
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_transit
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_modernizr
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_pace
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_webfontloader

php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_vh
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_h
php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_kueche

mv "_.htaccess" "web/.htaccess"
mv "404.php" "web/typo3conf/404.php"
mv "503.php" "web/typo3conf/503.php"
mv "db.ini" "web/typo3conf/db.ini"
mv "AdditionalConfiguration.php" "web/typo3conf/AdditionalConfiguration.php"