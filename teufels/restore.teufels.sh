#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../bin/.config.sh"

if [ "$#" -ne 1 ]; then
    echo "No type defined"
    exit 1
fi

mkdir -p -- "${BACKUP_DIR}"

case "$1" in    
    ###################################
    ## MySQL import to dev database
    ###################################
    "mysqlimport")
        if [ -f "${BACKUP_DIR}/mysql.sql" ]; then
            logMsg "Starting MySQL restore..."
            mysql -hmysql -udev -pdev dev < ${BACKUP_DIR}/mysql.sql
            logMsg "Finished"
        else
            errorMsg "MySQL database file not found"
            exit 1
        fi
        ;;

    ###################################
    ## MySQL import TYPO3 dummy to dev database
    ###################################
    "mysqlimporttypo3dummy")
        if [ -f "${BACKUP_DIR}/TYPO3dummy.sql" ]; then
            logMsg "Import TYPO3 dummy database..."
            mysql -hmysql -udev -pdev dev < ${BACKUP_DIR}/TYPO3dummy.sql
            logMsg "Finished"
        else
            errorMsg "TYPO3dummy.sql file not found"
            exit 1
        fi
        ;;         
esac
